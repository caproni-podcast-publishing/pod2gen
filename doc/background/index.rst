==========
Background
==========

Learn about the "why" and "how" of the pod2gen project itself.

.. toctree::
   :maxdepth: 1

   philosophy
   scope
   fork
   license
