=============
Why the fork?
=============

This project is a fork of python-podgen_. It follows the exact same syntax
but adds to it several other classes that models XML tags from the
`RSS Namespace Extension for Podcasting`_.

Projects using versions 1.1.0_ or before of python-podgen_ can safely move
to pod2gen_ and start using the new classes for building more specific and
advanced podcast feeds.

The goal of pod2gen_ remains the same as python-podgen_ which is
**making it easy and simple to generate podcasts** from a Python program
but intends to stay more up to date with latest `RSS Namespace Extension for Podcasting`_..

Migration from python-podgen to pod2gen
---------------------------------------
Python 2.7 support has been dropped. If you are using python-podgen_ with python 2.7
you might need to move to python 3.6+ before start using pod2gen_ but if you are already
using Python 3.6+ with python-podgen_, the transition will be simple. All you need to do
is replacing ``podgen`` with ``pod2gen`` in your imports

::

    # Old import statement
    # from podgen import Podcast

    # New import statement
    from pod2gen import Podcast

You can then start implementing new tags within the `RSS Namespace Extension for Podcasting`_.
by importing newly implemented classes from ``pod2gen``

::

    from pod2gen import (
        AlternateMedia,
        Funding,
        License,
        Location,
        Soundbite,
        Trailer,
        Transcript,
    )

Please refer to :doc:`api` for more details about how to use the new classes.

Summary of changes
------------------

* Dropping python 2.7 support
* Implementing `RSS Namespace Extension for Podcasting`_ tags

.. _python-podgen: https://github.com/tobinus/python-podgen
.. _1.1.0: https://github.com/tobinus/python-podgen/releases/tag/v1.1.0
.. _`RSS Namespace Extension for Podcasting`: https://podcastindex.org/namespace/1.0
.. _pod2gen: https://gitlab.com/caproni-podcast-publishing/pod2gen