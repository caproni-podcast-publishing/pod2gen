=======
pod2gen
=======

.. image:: https://gitlab.com/caproni-podcast-publishing/pod2gen/badges/master/pipeline.svg
   :target: https://gitlab.com/caproni-podcast-publishing/pod2gen/badges/master/pipeline.svg
   :alt: Continuous Integration (Gitlab CI)

.. image:: https://gitlab.com/caproni-podcast-publishing/pod2gen/badges/master/coverage.svg
   :target: https://gitlab.com/caproni-podcast-publishing/pod2gen/badges/master/coverage.svg
   :alt: Test Coverage (Gitlab CI)

.. image:: https://shields.io/pypi/v/pod2gen
   :target: https://shields.io/pypi/v/pod2gen
   :alt: Pypi version

.. image:: https://shields.io/pypi/pyversions/pod2gen
   :target: https://shields.io/pypi/pyversions/pod2gen
   :alt: Python version

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
    :target: https://github.com/psf/black
    :alt: Code Style: Black

.. image:: https://readthedocs.org/projects/pod2gen/badge/?version=latest
   :target: https://readthedocs.org/projects/pod2gen/badge/?version=latest
   :alt: Documentation Status

.. image:: https://shields.io/pypi/l/pod2gen
   :target: https://shields.io/pypi/l/pod2gen
   :alt: License

.. image:: _static/mascot.png
   :target: _static/mascot.png
   :alt: Podcasting 2.0


Are you looking for a **clean and simple library** which helps you
**generate podcast RSS feeds** with **the latest podcast namespace tags**
from your Python code? Here is how you do that with pod2gen, a fork from
PodGen implementing the https://podcastindex.org/namespace/1.0 namespace::

   from pod2gen import Podcast, Episode, Media
   # Create the Podcast
   p = Podcast(
      name="Animals Alphabetically",
      description="Every Tuesday, biologist John Doe and wildlife "
                  "photographer Foo Bar introduce you to a new animal.",
      website="http://example.org/animals-alphabetically",
      explicit=False,
   )
   # Add some episodes
   p.episodes += [
      Episode(
        title="Aardvark",
        media=Media("http://example.org/files/aardvark.mp3", 11932295),
        summary="With an English name adapted directly from Afrikaans "
                '-- literally meaning "earth pig" -- this fascinating '
                "animal has both circular teeth and a knack for "
                "digging.",
      ),
      Episode(
         title="Alpaca",
         media=Media("http://example.org/files/alpaca.mp3", 15363464),
         summary="Thousands of years ago, alpacas were already "
                 "domesticated and bred to produce the best fibers. "
                 "Case in point: we have found clothing made from "
                 "alpaca fiber that is 2000 years old. How is this "
                 "possible, and what makes it different from llamas?",
      ),
   ]
   # Generate the RSS feed
   rss = p.rss_str()

You don't need to read the RSS specification, write XML by hand or wrap your
head around ambiguous, undocumented APIs. pod2gen incorporates the industry's
best practices and lets you focus on collecting the necessary metadata and
publishing the podcast.

pod2gen is compatible with Python 3.6+.


Contents
--------

.. toctree::
   :maxdepth: 3

   background/index
   usage_guide/index
   advanced/index
   contributing
   api/index


External Resources
------------------

* `Changelog <https://gitlab.com/caproni-podcast-publishing/pod2gen/-/blob/master/CHANGELOG.md>`_
* `GitLab Repository <https://gitlab.com/caproni-podcast-publishing/pod2gen/-/tree/master>`_
* `Python Package Index <https://pypi.org/project/pod2gen/>`_
