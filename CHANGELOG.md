# Changelog

This library was built on top of [PodGen 1.1.0](https://pypi.org/project/podgen/).
All notable changes to this project will be documented in this file. The documented
changes follows the changes of the [PodGen](https://pypi.org/project/podgen/) project
up until the [1.1.0](https://github.com/tobinus/python-podgen/releases/tag/v1.1.0) release

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 1.0.3 - 2021-12-19
### Changed
- using "yes" and "no" for the <itunes:explicit> tag text instead of "Yes" and "No"

## 1.0.2 - 2021-10-31
### Added
- Implementation of missing features from [RSS Namespace Extension for Podcasting](https://podcastindex.org/namespace/1.0)
    - using multiple alternateEnclosure tags for episodes
    - adding the possibility to use episodes and seasons labels for display purposes  
    
## 1.0.1 - 2021-10-13
### Added
- Implementation of the [RSS Namespace Extension for Podcasting](https://podcastindex.org/namespace/1.0)

### Removed
- support for python 2.7. pod2gen was built and tested for python 3.6+.
