# -*- coding: utf-8 -*-
"""
    pod2gen.tests.test_trailer
    ~~~~~~~~~~~~~~~~~~~~~~~~

    Test the Trailer class, which represents a podcast Trailer.

    :copyright: 2021, Slim Beji <mslimbeji@gmail.com>
    :license: FreeBSD and LGPL, see license.* for more details.
"""
import unittest
from datetime import datetime

from dateutil import tz

from pod2gen import Trailer


class TestTrailer(unittest.TestCase):
    def setUp(self):
        self.text = "Coming April 1st, 2021"
        self.url = "https://example.org/trailers/teaser"
        self.pubdate = datetime(2021, 8, 15, 8, 15, 12, 0, tz.UTC)
        self.length = 12345678
        self.type = "audio/mp3"
        self.season = 2

    def _trailer(self):
        return Trailer(
            text=self.text,
            url=self.url,
            pubdate=self.pubdate,
            length=self.length,
            type=self.type,
            season=self.season,
        )

    def test_constructor(self):
        t = Trailer(self.text, self.url, self.pubdate)
        assert t.text == self.text
        assert t.url == self.url
        assert t.pubdate == self.pubdate
        assert t.length is None
        assert t.type is None
        assert t.season is None

        t = self._trailer()
        assert t.text == self.text
        assert t.url == self.url
        assert t.pubdate == self.pubdate
        assert t.length == self.length
        assert t.type == self.type
        assert t.season == self.season

    def test_constructorMissingParameter(self):
        self.assertRaises(TypeError, Trailer, "param_1")

    def test_settingText(self):
        other_text = "David Gilmour is awsome!"
        t = self._trailer()
        t.text = other_text
        assert t.text == other_text
        assert t.url == self.url
        assert t.pubdate == self.pubdate
        assert t.length == self.length
        assert t.type == self.type
        assert t.season == self.season

    def test_wrongText(self):
        self.assertRaises(ValueError, Trailer, 0, self.url, self.pubdate)
        self.assertRaises(ValueError, Trailer, None, self.url, self.pubdate)
        self.assertRaises(ValueError, Trailer, "", self.url, self.pubdate)
        self.assertRaises(ValueError, Trailer, "0" * 129, self.url, self.pubdate)

    def test_settingUrl(self):
        other_url = "https://example.org/trailers/teaser_v2"
        t = self._trailer()
        t.url = other_url
        assert t.text == self.text
        assert t.url == other_url
        assert t.pubdate == self.pubdate
        assert t.length == self.length
        assert t.type == self.type
        assert t.season == self.season

    def test_wrongUrl(self):
        self.assertRaises(ValueError, Trailer, self.text, "", self.pubdate)
        self.assertRaises(ValueError, Trailer, self.text, None, self.pubdate)
        self.assertRaises(
            ValueError, Trailer, self.text, "Not an actual url", self.pubdate
        )

    def test_settingPubdate(self):
        other_pubdate = datetime(2021, 9, 15, 8, 15, 12, 0, tz.UTC)
        t = self._trailer()
        t.pubdate = other_pubdate
        assert t.text == self.text
        assert t.url == self.url
        assert t.pubdate == other_pubdate
        assert t.length == self.length
        assert t.type == self.type
        assert t.season == self.season

    def test_wrongPubdate(self):
        t = self._trailer()
        self.assertRaises(ValueError, setattr, t, "pubdate", None)
        self.assertRaises(TypeError, setattr, t, "pubdate", "a string")
        self.assertRaises(TypeError, setattr, t, "pubdate", 33)
        self.assertRaises(
            ValueError, setattr, t, "pubdate", self.pubdate.replace(tzinfo=None)
        )

    def test_pubdateToText(self):
        t = self._trailer()
        pubdate = datetime(1990, 9, 28, 8, 16, 30, tzinfo=tz.UTC)
        t.pubdate = pubdate
        assert t.pubdate == pubdate
        assert t.rss_entry().attrib["pubdate"] == "Fri, 28 Sep 1990 08:16:30 +0000"

    def test_settingLength(self):
        other_length = 87654321
        t = self._trailer()
        t.length = other_length
        assert t.text == self.text
        assert t.url == self.url
        assert t.pubdate == self.pubdate
        assert t.length == other_length
        assert t.type == self.type
        assert t.season == self.season

    def test_wrongLength(self):
        t = self._trailer()
        self.assertRaises(TypeError, setattr, t, "length", "not integer")

    def test_settingType(self):
        other_type = "audio/mp4"
        t = self._trailer()
        t.type = other_type
        assert t.text == self.text
        assert t.url == self.url
        assert t.pubdate == self.pubdate
        assert t.length == self.length
        assert t.type == other_type
        assert t.season == self.season

    def test_wrongType(self):
        t = self._trailer()
        self.assertRaises(TypeError, setattr, t, "type", 55)

    def test_settingSeason(self):
        other_season = 4
        t = self._trailer()
        t.season = other_season
        assert t.text == self.text
        assert t.url == self.url
        assert t.pubdate == self.pubdate
        assert t.length == self.length
        assert t.type == self.type
        assert t.season == other_season

    def test_wrongSeason(self):
        t = self._trailer()
        self.assertRaises(TypeError, setattr, t, "season", "not integer")
