# -*- coding: utf-8 -*-
"""
    pod2gen.tests.test_soundbite
    ~~~~~~~~~~~~~~~~~~~~~~~~

    Test the Soundbite class, which represents an episode soundbite.

    :copyright: 2021, Slim Beji <mslimbeji@gmail.com>
    :license: FreeBSD and LGPL, see license.* for more details.
"""
import unittest

from pod2gen import Soundbite


class TestSoundbite(unittest.TestCase):
    def setUp(self):
        self.start_time = 1234.5
        self.duration = 42.25
        self.text = "Why the Podcast Namespace Matters"

    def _soundbite(self):
        return Soundbite(self.start_time, self.duration, text=self.text)

    def test_constructorWithoutText(self):
        s = Soundbite(self.start_time, self.duration)
        assert s.start_time == self.start_time
        assert s.duration == self.duration
        assert s.text is None

    def test_constructorMissingParameter(self):
        self.assertRaises(TypeError, Soundbite, "param_1")

    def test_constructorWithText(self):
        s = Soundbite(self.start_time, self.duration)
        assert s.start_time == self.start_time
        assert s.duration == self.duration
        assert s.text is None

    def test_settingStartTime(self):
        new_start_time = 9999
        assert new_start_time != self.start_time
        s = self._soundbite()
        s.start_time = new_start_time
        assert s.start_time == new_start_time
        assert s.duration == self.duration
        assert s.text == self.text

    def test_settingDuration(self):
        new_duration = 8888
        assert new_duration != self.duration
        s = self._soundbite()
        s.duration = new_duration
        assert s.start_time == self.start_time
        assert s.duration == new_duration
        assert s.text == self.text

    def test_settingText(self):
        new_text = "An updated soundbite description"
        assert new_text != self.text
        s = self._soundbite()
        s.text = new_text
        assert s.start_time == self.start_time
        assert s.duration == self.duration
        assert s.text == new_text

    def test_wrongStartTime(self):
        self.assertRaises(ValueError, Soundbite, "a", self.duration)
        self.assertRaises(ValueError, Soundbite, None, self.duration)

    def test_wrongDuration(self):
        self.assertRaises(ValueError, Soundbite, self.start_time, "a")
        self.assertRaises(ValueError, Soundbite, self.start_time, None)

    def test_wrongText(self):
        self.assertRaises(ValueError, Soundbite, self.start_time, self.duration, text=1)
        self.assertRaises(
            ValueError, Soundbite, self.start_time, self.duration, text="a" * 129
        )
