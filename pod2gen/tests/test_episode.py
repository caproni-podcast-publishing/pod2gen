# -*- coding: utf-8 -*-
"""
    pod2gen.tests.test_episode
    ~~~~~~~~~~~~~~~~~~~~~~~~~

    Test the Episode class.

    :copyright: 2016, Thorben Dahl <thorben@sjostrom.no>
        2021, Slim Beji <mslimbeji@gmail.com>
    :license: FreeBSD and LGPL, see license.* for more details.
"""
import datetime
import unittest
import warnings

from dateutil import tz
from dateutil.parser import parse as parsedate

from pod2gen import (
    EPISODE_TYPE_BONUS,
    EPISODE_TYPE_FULL,
    EPISODE_TYPE_TRAILER,
    AlternateMedia,
    Episode,
    License,
    Location,
    Media,
    NotSupportedByItunesWarning,
    Person,
    Podcast,
    Soundbite,
    Transcript,
    htmlencode,
)


class TestBaseEpisode(unittest.TestCase):
    def setUp(self):

        self.itunes_ns = "http://www.itunes.com/dtds/podcast-1.0.dtd"
        self.dublin_ns = "http://purl.org/dc/elements/1.1/"
        self.podcast_ns = "https://podcastindex.org/namespace/1.0"

        fg = Podcast()
        self.title = "Some Testfeed"
        self.link = "http://lernfunk.de"
        self.description = "A cool tent"
        self.explicit = False

        fg.name = self.title
        fg.website = self.link
        fg.description = self.description
        fg.explicit = self.explicit

        fe = fg.add_episode()
        fe.id = "http://lernfunk.de/media/654321/1"
        fe.title = "The First Episode"
        fe.chapters_json = "https://example.com/episode1/chapters.json"
        self.license = License(
            "my-podcast-license-v1", "https://example.org/mypodcastlicense/full.pdf"
        )
        fe.license = self.license

        self.soundbite_start_time = 1234.5
        self.soundbite_duration = 42.25
        self.soundbite_text = "Why the Podcast Namespace Matters"
        self.soundbite = Soundbite(
            self.soundbite_start_time, self.soundbite_duration, self.soundbite_text
        )
        fe.add_soundbite(self.soundbite)

        self.person_name = "Becky Smith"
        self.person_group = "visuals"
        self.person_role = "Cover Art Designer"
        self.person_href = "https://example.com/artist/beckysmith"
        self.person_img = "http://example.com/images/alicebrown.jpg"
        self.person = Person(
            name=self.person_name,
            group=self.person_group,
            role=self.person_role,
            href=self.person_href,
            img=self.person_img,
        )
        fe.add_person(self.person)

        self.fe = fe

        # Use also the list directly
        fe = Episode()
        fg.episodes.append(fe)
        fe.id = "http://lernfunk.de/media/654321/2"
        fe.title = "The Second Episode"

        fe = fg.add_episode()
        fe.id = "http://lernfunk.de/media/654321/3"
        fe.title = "The Third Episode"

        self.alternate_media = AlternateMedia(
            "audio/mp4",
            43200000,
            bitrate=128000,
            height=1080,
            lang="en-US",
            title="Standard",
            rel="Off stage",
            codecs="mp4a.40.2",
            default=False,
            encryption="sri",
            signature="sha384-ExVqijgYHm15PqQqdXfW95x+Rs6C+d6E/ICxyQOeFevnxNLR/wtJNrNYTjIysUBo",
        )
        self.alternate_media.sources = {
            "https://example.com/file-0.mp3": None,
            "ipfs://QmdwGqd3d2gFPGeJNLLCshdiPert45fMu84552Y4XHTy4y": None,
            "https://example.com/file-0.torrent": "application/x-bittorrent",
        }
        fe.alternate_media_list = [self.alternate_media]

        self.fg = fg

    def test_constructor(self):
        title = "A constructed episode"
        subtitle = "We're using the constructor!"
        summary = (
            "In this week's episode, we try using the constructor to "
            "create a new Episode object."
        )
        long_summary = (
            "In this week's episode, we try to use the constructor "
            "to create a new Episode object. Additionally, we'll "
            "check whether it actually worked or not. Hold your "
            "fingers crossed!"
        )
        media = Media(
            "http://example.com/episodes/1.mp3",
            1425345346,
            "audio/mpeg",
            datetime.timedelta(hours=1, minutes=2, seconds=22),
        )
        publication_date = datetime.datetime(2016, 6, 7, 13, 37, 0, tzinfo=tz.UTC)
        link = "http://example.com/blog/?i=1"
        authors = [Person("John Doe", "johndoe@example.com")]
        image = "http://example.com/static/1.png"
        explicit = True
        is_closed_captioned = False
        position = 3
        withhold_from_itunes = True
        season = 5
        season_name = "Volume 5"
        episode_number = 4
        episode_name = "Chapter 4"
        chapters_json = "https://other-example.com/episode1/chapters.json"

        ep = Episode(
            title=title,
            subtitle=subtitle,
            summary=summary,
            long_summary=long_summary,
            media=media,
            alternate_media_list=[self.alternate_media],
            publication_date=publication_date,
            link=link,
            authors=authors,
            image=image,
            explicit=explicit,
            is_closed_captioned=is_closed_captioned,
            position=position,
            withhold_from_itunes=withhold_from_itunes,
            season=season,
            season_name=season_name,
            episode_number=episode_number,
            episode_name=episode_name,
            chapters_json=chapters_json,
            soundbites=[self.soundbite],
            persons=[self.person],
            license=self.license,
        )

        # Time to check if this works
        self.assertEqual(ep.title, title)
        self.assertEqual(ep.subtitle, subtitle)
        self.assertEqual(ep.summary, summary)
        self.assertEqual(ep.long_summary, long_summary)
        self.assertEqual(ep.media, media)
        self.assertEqual(ep.alternate_media_list, [self.alternate_media])
        self.assertEqual(ep.publication_date, publication_date)
        self.assertEqual(ep.link, link)
        self.assertEqual(ep.authors, authors)
        self.assertEqual(ep.image, image)
        self.assertEqual(ep.explicit, explicit)
        self.assertEqual(ep.is_closed_captioned, is_closed_captioned)
        self.assertEqual(ep.position, position)
        self.assertEqual(ep.withhold_from_itunes, withhold_from_itunes)
        self.assertEqual(ep.season, season)
        self.assertEqual(ep.season_name, season_name)
        self.assertEqual(ep.episode_number, episode_number)
        self.assertEqual(ep.episode_name, episode_name)
        self.assertEqual(ep.chapters_json, chapters_json)
        self.assertEqual(ep.soundbites, [self.soundbite])
        self.assertEqual(ep.persons, [self.person])
        self.assertEqual(ep.license, self.license)

    def test_constructorUnknownKeyword(self):
        self.assertRaises(TypeError, Episode, tittel="What is tittel")
        self.assertRaises(TypeError, Episode, "This is not a keyword")

    def test_checkItemNumbers(self):
        fg = self.fg
        assert len(fg.episodes) == 3

    def test_checkEntryContent(self):
        fg = self.fg
        assert len(fg.episodes) is not None

    def test_removeEntryByIndex(self):
        fg = Podcast()
        self.feedId = "http://example.com"
        self.title = "Some Testfeed"

        fe = fg.add_episode()
        fe.id = "http://lernfunk.de/media/654321/1"
        fe.title = "The Third BaseEpisode"
        assert len(fg.episodes) == 1
        fg.episodes.pop(0)
        assert len(fg.episodes) == 0

    def test_removeEntryByEntry(self):
        fg = Podcast()
        self.feedId = "http://example.com"
        self.title = "Some Testfeed"

        fe = fg.add_episode()
        fe.id = "http://lernfunk.de/media/654321/1"
        fe.title = "The Third BaseEpisode"

        assert len(fg.episodes) == 1
        fg.episodes.remove(fe)
        assert len(fg.episodes) == 0

    def test_idIsSet(self):
        guid = "http://example.com/podcast/episode1"
        episode = Episode()
        episode.title = "My first episode"
        episode.id = guid
        item = episode.rss_entry()

        assert item.find("guid").text == guid

    def test_idNotSetButEnclosureIsUsed(self):
        guid = "http://example.com/podcast/episode1.mp3"
        episode = Episode()
        episode.title = "My first episode"
        episode.media = Media(guid, 97423487, "audio/mpeg")

        item = episode.rss_entry()
        assert item.find("guid").text == guid

    def test_idSetToFalseSoEnclosureNotUsed(self):
        episode = Episode()
        episode.title = "My first episode"
        episode.media = Media(
            "http://example.com/podcast/episode1.mp3", 34328731, "audio/mpeg"
        )
        episode.id = False

        item = episode.rss_entry()
        assert item.find("guid") is None

    def test_feedPubDateUsesNewestEpisode(self):
        self.fg.episodes[0].publication_date = datetime.datetime(
            2015, 1, 1, 15, 0, tzinfo=tz.UTC
        )
        self.fg.episodes[1].publication_date = datetime.datetime(
            2016, 1, 3, 12, 22, tzinfo=tz.UTC
        )
        self.fg.episodes[2].publication_date = datetime.datetime(
            2014, 3, 2, 13, 11, tzinfo=tz.UTC
        )
        rss = self.fg._create_rss()
        pubDate = rss.find("channel").find("pubDate")
        assert pubDate is not None
        parsedPubDate = parsedate(pubDate.text)
        assert parsedPubDate == self.fg.episodes[1].publication_date

    def test_feedPubDateNotOverriddenByEpisode(self):
        self.fg.episodes[0].publication_date = datetime.datetime(
            2015, 1, 1, 15, 0, tzinfo=tz.UTC
        )
        pubDate = self.fg._create_rss().find("channel").find("pubDate")
        # Now it uses the episode's published date
        assert pubDate is not None
        assert parsedate(pubDate.text) == self.fg.episodes[0].publication_date

        new_date = datetime.datetime(2016, 1, 2, 3, 4, tzinfo=tz.UTC)
        self.fg.publication_date = new_date
        pubDate = self.fg._create_rss().find("channel").find("pubDate")
        # Now it uses the custom-set date
        assert pubDate is not None
        assert parsedate(pubDate.text) == new_date

    def test_feedPubDateDisabled(self):
        self.fg.episodes[0].publication_date = datetime.datetime(
            2015, 1, 1, 15, 0, tzinfo=tz.UTC
        )
        self.fg.publication_date = False
        pubDate = self.fg._create_rss().find("channel").find("pubDate")
        assert pubDate is None  # Not found!

    def test_oneAuthor(self):
        name = "John Doe"
        email = "johndoe@example.org"
        self.fe.authors = [Person(name, email)]
        author_text = self.fe.rss_entry().find("author").text
        assert name in author_text
        assert email in author_text

        # Test that itunes:author is the name
        assert self.fe.rss_entry().find("{%s}author" % self.itunes_ns).text == name
        # Test that dc:creator is not used when rss author does the same job
        assert self.fe.rss_entry().find("{%s}creator" % self.dublin_ns) is None

    def test_oneAuthorWithoutEmail(self):
        name = "John Doe"
        self.fe.authors.append(Person(name))
        entry = self.fe.rss_entry()

        # Test that author is not used, since it requires email
        assert entry.find("author") is None
        # Test that itunes:author is still the name
        assert entry.find("{%s}author" % self.itunes_ns).text == name
        # Test that dc:creator is used in rss author's place (since dc:creator
        # doesn't require email)
        assert entry.find("{%s}creator" % self.dublin_ns).text == name

    def test_oneAuthorWithoutName(self):
        email = "johndoe@example.org"
        self.fe.authors.extend([Person(email=email)])
        entry = self.fe.rss_entry()

        # Test that rss author is the email
        assert entry.find("author").text == email
        # Test that itunes:author is not used, since it requires name
        assert entry.find("{%s}author" % self.itunes_ns) is None
        # Test that dc:creator is not used, since it would duplicate rss author
        assert entry.find("{%s}creator" % self.dublin_ns) is None

    def test_multipleAuthors(self):
        person1 = Person("John Doe", "johndoe@example.org")
        person2 = Person("Mary Sue", "marysue@example.org")

        self.fe.authors = [person1, person2]
        author_elements = self.fe.rss_entry().findall("{%s}creator" % self.dublin_ns)
        author_texts = [e.text for e in author_elements]

        # Test that both authors are included, in the same order they were added
        assert person1.name in author_texts[0]
        assert person1.email in author_texts[0]
        assert person2.name in author_texts[1]
        assert person2.email in author_texts[1]

        # Test that itunes:author includes all authors' name, but not email
        itunes_author = self.fe.rss_entry().find("{%s}author" % self.itunes_ns).text
        assert person1.name in itunes_author
        assert person1.email not in itunes_author
        assert person2.name in itunes_author
        assert person2.email not in itunes_author

        # Test that the regular rss tag is not used, per the RSS recommendations
        assert self.fe.rss_entry().find("author") is None

    def test_authorsInvalidAssignment(self):
        self.assertRaises(TypeError, self.do_authorsInvalidAssignment)

    def do_authorsInvalidAssignment(self):
        self.fe.authors = Person("Oh No", "notan@iterable.no")

    def test_media(self):
        media = Media("http://example.org/episodes/1.mp3", 14536453, "audio/mpeg")
        self.fe.media = media
        enclosure = self.fe.rss_entry().find("enclosure")

        self.assertEqual(enclosure.get("url"), media.url)
        self.assertEqual(enclosure.get("length"), str(media.size))
        self.assertEqual(enclosure.get("type"), media.type)

        # Ensure duck-typing is checked at assignment time
        self.assertRaises(TypeError, setattr, self.fe, "media", media.url)
        self.assertRaises(
            TypeError, setattr, self.fe, "media", (media.url, media.size, media.type)
        )

    def test_setAlternateMedia(self):
        self.fe.alternate_media_list = None
        assert self.fe.alternate_media_list == []

        self.fe.add_alternate_media(self.alternate_media)
        assert self.fe.alternate_media_list == [self.alternate_media]

    def test_setWrongAlternateMedia(self):
        # Using the add_alternate_media method
        self.assertRaises(TypeError, self.fe.add_alternate_media, object)
        self.assertRaises(TypeError, self.fe.add_alternate_media, 0)
        self.assertRaises(TypeError, self.fe.add_alternate_media, "")
        self.assertRaises(TypeError, self.fe.add_alternate_media, "sqsqs")

        # Setting the self.alternate_media_list attribute with an invalid item
        self.assertRaises(
            TypeError,
            setattr,
            self.fe,
            "alternate_media_list",
            [self.alternate_media, object],
        )
        self.assertRaises(
            TypeError,
            setattr,
            self.fe,
            "alternate_media_list",
            [self.alternate_media, 0],
        )
        self.assertRaises(
            TypeError,
            setattr,
            self.fe,
            "alternate_media_list",
            [self.alternate_media, ""],
        )
        self.assertRaises(
            TypeError,
            setattr,
            self.fe,
            "alternate_media_list",
            [self.alternate_media, "sqsqs"],
        )

    def test_withholdFromItunesOffByDefault(self):
        assert not self.fe.withhold_from_itunes

    def test_withholdFromItunes(self):
        self.fe.withhold_from_itunes = True
        itunes_block = self.fe.rss_entry().find("{%s}block" % self.itunes_ns)
        assert itunes_block is not None
        self.assertEqual(itunes_block.text.lower(), "yes")

        self.fe.withhold_from_itunes = False
        itunes_block = self.fe.rss_entry().find("{%s}block" % self.itunes_ns)
        assert itunes_block is None

    def test_summaries(self):
        content_encoded = "{%s}encoded" % "http://purl.org/rss/1.0/modules/content/"
        # Test that none are in use by default (no summary is set)
        d = self.fe.rss_entry().find("description")
        assert d is None
        ce = self.fe.rss_entry().find(content_encoded)
        assert ce is None

        # Test that description is filled when one of the summaries is set
        self.fe.summary = "A short summary"
        d = self.fe.rss_entry().find("description")
        assert d is not None
        assert "A short summary" == d.text
        ce = self.fe.rss_entry().find(content_encoded)
        assert ce is None

        self.fe.summary = False
        self.fe.long_summary = "A long summary with more words"
        d = self.fe.rss_entry().find("description")
        assert d is not None
        assert "A long summary with more words" == d.text
        ce = self.fe.rss_entry().find(content_encoded)
        assert ce is None

        # Test that description and content:encoded are used when both are set
        self.fe.summary = "A short summary"
        self.fe.long_summary = "A long summary with more words"
        d = self.fe.rss_entry().find("description")
        assert d is not None
        assert "A short summary" == d.text
        ce = self.fe.rss_entry().find(content_encoded)
        assert ce is not None
        assert "A long summary with more words" == ce.text

    def test_summariesHtml(self):
        self.fe.summary = "A <b>cool</b> summary"
        d = self.fe.rss_entry().find("description")
        assert d is not None
        assert "A <b>cool</b> summary" == d.text

        self.fe.summary = htmlencode("A <b>cool</b> summary")
        d = self.fe.rss_entry().find("description")
        assert d is not None
        assert "A &lt;b&gt;cool&lt;/b&gt; summary" == d.text

    def test_position(self):
        # Test that position is set (testing Podcast and Episode)
        self.fg.apply_episode_order()
        self.assertEqual(self.fg.episodes[0].position, 1)
        self.assertEqual(self.fg.episodes[1].position, 2)
        self.assertEqual(self.fg.episodes[2].position, 3)

        # Test that position is also output as part of RSS (testing Episode)
        itunes_order = self.fe.rss_entry().find("{%s}order" % self.itunes_ns)
        assert itunes_order is not None
        self.assertEqual(itunes_order.text, str(self.fe.position))

        # Test that clearing works (testing Podcast and Episode)
        self.fg.clear_episode_order()
        assert self.fg.episodes[0].position is None
        assert self.fg.episodes[1].position is None
        assert self.fg.episodes[2].position is None

        # No longer itunes:order element (testing Episode)
        itunes_order = self.fe.rss_entry().find("{%s}order" % self.itunes_ns)
        assert itunes_order is None

    def test_episodeNumber(self):
        # Don't appear if None (default)
        assert self.fe.episode_number is None
        assert self.fe.rss_entry().find("{%s}episode" % self.itunes_ns) is None
        assert self.fe.rss_entry().find("{%s}episode" % self.podcast_ns) is None

        # Appear with the right number when set
        self.fe.episode_number = 1
        assert self.fe.episode_number == 1
        itunes_episode = self.fe.rss_entry().find("{%s}episode" % self.itunes_ns)
        podcast_episode = self.fe.rss_entry().find("{%s}episode" % self.podcast_ns)
        assert itunes_episode is not None
        assert itunes_episode.text == "1"
        assert podcast_episode is not None
        assert podcast_episode.text == "1"

        # Must be non-zero integer
        self.assertRaises(ValueError, setattr, self.fe, "episode_number", 0)
        self.assertRaises(ValueError, setattr, self.fe, "episode_number", -1)
        self.assertRaises(
            ValueError, setattr, self.fe, "episode_number", "not a number"
        )
        assert self.fe.episode_number == 1

    def test_episodeNumberMandatoryWhenSerial(self):
        # Vary the first episode. Ensure the remaining two don't interfere
        for i, episode in enumerate(self.fg.episodes[1:], start=2):
            episode.episode_number = i

        # Test that the missing episode_number is reacted on
        self.fg.is_serial = True
        self.assertRaises((RuntimeError, ValueError), self.fg.rss_str)

        # Does not raise when the episode is a trailer
        self.fe.episode_type = EPISODE_TYPE_TRAILER
        self.fg.rss_str()

        # Does not raise when the episode is a bonus
        self.fe.episode_type = EPISODE_TYPE_BONUS
        self.fg.rss_str()

        # Still raises for full episode
        self.fe.episode_type = EPISODE_TYPE_FULL
        self.assertRaises((RuntimeError, ValueError), self.fg.rss_str)

        # Does not raise when the episode has a number
        self.fe.episode_number = 1
        self.fg.rss_str()

    def test_episodeName(self):
        self.fe.episode_number = 1
        self.fe.episode_name = "Chapter 01"
        assert self.fe.episode_number == 1
        assert self.fe.episode_name == "Chapter 01"
        assert (
            self.fe.rss_entry().find("{%s}episode" % self.podcast_ns).attrib["display"]
            == "Chapter 01"
        )

        # Must be a string
        self.assertRaises(ValueError, setattr, self.fe, "episode_name", 0)
        self.assertRaises(ValueError, setattr, self.fe, "episode_name", 7)
        self.assertRaises(ValueError, setattr, self.fe, "episode_name", object)

        # Must not exceed 128 characters
        self.assertRaises(ValueError, setattr, self.fe, "episode_name", "a" * 129)

    def test_mandatoryAttributes(self):
        ep = Episode()
        self.assertRaises((RuntimeError, ValueError), ep.rss_entry)

        ep.title = "A title"
        ep.rss_entry()

        ep.title = ""
        self.assertRaises((RuntimeError, ValueError), ep.rss_entry)

        ep.title = None
        self.assertRaises((RuntimeError, ValueError), ep.rss_entry)

        ep.summary = "A summary"
        ep.rss_entry()

        ep.summary = ""
        self.assertRaises((RuntimeError, ValueError), ep.rss_entry)

        ep.summary = None
        self.assertRaises((RuntimeError, ValueError), ep.rss_entry)

    def test_explicit(self):
        # Don't appear if None (use podcast's explicit value)
        assert self.fe.explicit is None
        assert self.fe.rss_entry().find("{%s}explicit" % self.itunes_ns) is None

        # Appear and say it's explicit if True
        self.fe.explicit = True
        itunes_explicit = self.fe.rss_entry().find("{%s}explicit" % self.itunes_ns)
        assert itunes_explicit is not None
        assert itunes_explicit.text in ("yes", "explicit", "true")

        # Appear and say it's clean if False
        self.fe.explicit = False
        itunes_explicit = self.fe.rss_entry().find("{%s}explicit" % self.itunes_ns)
        assert itunes_explicit is not None
        assert itunes_explicit.text in ("no", "clean", "false")

    def test_image(self):
        # Test that the attribute works
        assert self.fe.image is None

        image = "https://static.example.org/img/hello.png"
        self.fe.image = image
        self.assertEqual(self.fe.image, image)

        # Test that it appears in XML
        itunes_image = self.fe.rss_entry().find("{%s}image" % self.itunes_ns)
        assert itunes_image is not None

        # Test that its contents is correct
        self.assertEqual(itunes_image.get("href"), image)
        assert itunes_image.text is None

    def test_imageWarningNoExt(self):
        # Set image to a URL without proper file extension
        no_ext = "http://static.example.com/images/logo"
        self.assertWarns(NotSupportedByItunesWarning, setattr, self.fe, "image", no_ext)
        self.assertEqual(no_ext, self.fe.image)

    def test_imageWarningBadExt(self):
        # Set image to a URL with an unsupported file extension
        bad_ext = "http://static.example.com/images/logo.gif"
        self.assertWarns(
            NotSupportedByItunesWarning, setattr, self.fe, "image", bad_ext
        )
        self.assertEqual(bad_ext, self.fe.image)

    def test_imageNoWarningWithGoodExt(self):
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("always")

            # Set image to a URL with a supported file extension
            extensions = ["jpg", "png", "jpeg"]
            for extension in extensions:
                good_ext = "http://static.example.com/images/logo." + extension
                self.fe.image = good_ext
                # Did we get no warning?
                self.assertEqual(
                    0, len(w), "Extension %s raised warnings (%s)" % (extension, w)
                )
                # Was the image set?
                self.assertEqual(good_ext, self.fe.image)

    def test_isClosedCaptioned(self):
        def get_element():
            return self.fe.rss_entry().find("{%s}isClosedCaptioned" % self.itunes_ns)

        # Starts out as False or None
        assert (
            self.fe.is_closed_captioned is None or self.fe.is_closed_captioned is False
        )

        # Not used when set to False
        self.fe.is_closed_captioned = False
        self.assertEqual(self.fe.is_closed_captioned, False)
        assert get_element() is None

        # Not used when set to None
        self.fe.is_closed_captioned = None
        assert self.fe.is_closed_captioned is None
        assert get_element() is None

        # Used and says "yes" when set to True
        self.fe.is_closed_captioned = True
        self.assertEqual(self.fe.is_closed_captioned, True)
        assert get_element() is not None
        self.assertEqual(get_element().text.lower(), "yes")

    def test_season(self):
        # Starts out as None
        assert self.fe.season is None

        # Not used when set to None
        assert self.fe.rss_entry().find("{%s}season" % self.itunes_ns) is None
        assert self.fe.rss_entry().find("{%s}season" % self.podcast_ns) is None

        # Can be set
        self.fe.season = 3
        self.assertEqual(self.fe.season, 3)

        # Is used when set
        itunes_season = self.fe.rss_entry().find("{%s}season" % self.itunes_ns)
        assert itunes_season is not None
        assert itunes_season.text == str(3)
        podcast_season = self.fe.rss_entry().find("{%s}season" % self.podcast_ns)
        assert podcast_season is not None
        assert podcast_season.text == str(3)

        # Can be reset to None
        self.fe.season = None
        assert self.fe.season is None
        itunes_season = self.fe.rss_entry().find("{%s}season" % self.itunes_ns)
        assert itunes_season is None
        podcast_season = self.fe.rss_entry().find("{%s}season" % self.podcast_ns)
        assert podcast_season is None

        # Gives error when set to something that cannot be converted to int
        with self.assertRaises(ValueError):
            self.fe.season = "Best Season"

        # Gives error when set to zero
        with self.assertRaises(ValueError):
            self.fe.season = 0

        # Gives error when set to negative number
        with self.assertRaises(ValueError):
            self.fe.season = -1

    def test_seasonDisplay(self):
        self.fe.season = 1
        self.fe.season_name = "Volume 01"
        assert self.fe.season == 1
        assert self.fe.season_name == "Volume 01"
        assert (
            self.fe.rss_entry().find("{%s}season" % self.podcast_ns).attrib["name"]
            == "Volume 01"
        )

        # Must be a string
        self.assertRaises(ValueError, setattr, self.fe, "season_name", 0)
        self.assertRaises(ValueError, setattr, self.fe, "season_name", 7)
        self.assertRaises(ValueError, setattr, self.fe, "season_name", object)

        # Cannot exceed 128 characters
        self.assertRaises(ValueError, setattr, self.fe, "season_name", "a" * 129)

    def test_chapters_json(self):
        with self.assertRaises(ValueError):
            self.fe.chapters_json = "not a valid url"

    def test_soundbites(self):
        # Test the soundbite tag
        soundbite_element = self.fe.rss_entry().find("{%s}soundbite" % self.podcast_ns)
        assert soundbite_element.text == self.soundbite_text

    def test_license(self):
        # Test the license tag
        license_element = self.fe.rss_entry().find("{%s}license" % self.podcast_ns)
        assert license_element.text == self.license.identifier

    def test_persons(self):
        # Test the person tag
        person_element = self.fe.rss_entry().find("{%s}person" % self.podcast_ns)
        assert person_element.text == self.person_name

    def test_episodeType(self):
        def get_element():
            return self.fe.rss_entry().find("{%s}episodeType" % self.itunes_ns)

        # Starts out as "full"
        self.assertEqual(self.fe.episode_type, EPISODE_TYPE_FULL)

        # Not used when set to "full"
        assert get_element() is None

        # Used when set to "trailer"
        self.fe.episode_type = EPISODE_TYPE_TRAILER
        self.assertEqual(self.fe.episode_type, EPISODE_TYPE_TRAILER)
        assert get_element() is not None
        self.assertEqual(get_element().text.lower(), "trailer")

        # Used when set to "bonus"
        self.fe.episode_type = EPISODE_TYPE_BONUS
        self.assertEqual(self.fe.episode_type, EPISODE_TYPE_BONUS)
        assert get_element() is not None
        self.assertEqual(get_element().text.lower(), "bonus")

        # Can be set to something that evaluates to "trailer" or "bonus" when
        # converted to str()
        class IsEpisodeTypeWhenStr:
            def __str__(self):
                return EPISODE_TYPE_TRAILER

        self.fe.episode_type = IsEpisodeTypeWhenStr()
        self.assertEqual(self.fe.episode_type, EPISODE_TYPE_TRAILER)
        assert get_element() is not None
        self.assertEqual(get_element().text.lower(), "trailer")

        # Fails when set to anything else
        with self.assertRaises(ValueError):
            self.fe.episode_type = "banana"

        with self.assertRaises(ValueError):
            self.fe.episode_type = False

    def test_link(self):
        def get_element():
            return self.fe.rss_entry().find("link")

        # Starts out as None or empty
        assert self.fe.link is None or self.fe.link == ""

        # Not used when set to None
        self.fe.link = None
        assert self.fe.link is None
        assert get_element() is None

        # Not used when set to empty
        self.fe.link = ""
        assert self.fe.link == ""
        assert get_element() is None

        # Used when set to something
        link = "http://example.com/episode1.html"
        self.fe.link = link
        self.assertEqual(self.fe.link, link)
        assert get_element() is not None
        self.assertEqual(get_element().text, link)

    def test_subtitle(self):
        def get_element():
            return self.fe.rss_entry().find("{%s}subtitle" % self.itunes_ns)

        # Starts out as None or empty
        assert self.fe.subtitle is None or self.fe.subtitle == ""

        # Not used when set to None
        self.fe.subtitle = None
        assert self.fe.subtitle is None
        assert get_element() is None

        # Not used when set to empty
        self.fe.subtitle = ""
        self.assertEqual(self.fe.subtitle, "")
        assert get_element() is None

        # Used when set to something
        subtitle = "This is a subtitle"
        self.fe.subtitle = subtitle
        self.assertEqual(self.fe.subtitle, subtitle)
        element = get_element()
        assert element is not None
        self.assertEqual(element.text, subtitle)

    def test_title(self):
        ep = Episode()

        def get_element():
            return ep.rss_entry().find("title")

        # Starts out as None or empty.
        assert ep.title is None or ep.title == ""

        # We test that you cannot create RSS when it's empty or blank in
        # another method.

        # Test that it is set correctly
        ep.title = None
        assert ep.title is None

        ep.title = ""
        self.assertEqual(ep.title, "")

        # Test that the title is used correctly
        title = "My Fine Title"
        ep.title = title
        self.assertEqual(ep.title, title)

        element = get_element()
        assert element is not None
        self.assertEqual(element.text, title)

    def test_addTranscript(self):
        fe = self.fe
        transcript_tags_number = len(fe.transcripts)

        t1 = Transcript(
            "https://examples.com/transcript_sample.txt",
            "text/html",
            language="es",
            is_caption=True,
        )
        fe.add_transcript(t1)
        assert len(fe.transcripts) == transcript_tags_number + 1

        t2 = Transcript("https://examples.com/transcript_sample_2.txt", "text/html")
        fe.add_transcript(t2)
        assert len(fe.transcripts) == transcript_tags_number + 2

    def test_removeTranscriptByIndex(self):
        fe = self.fe
        transcript_tags_number = len(fe.transcripts)

        t1 = Transcript(
            "https://examples.com/transcript_sample.txt",
            "text/html",
            language="es",
            is_caption=True,
        )
        fe.add_transcript(t1)
        assert len(fe.transcripts) == transcript_tags_number + 1

        fe.transcripts.pop(0)
        assert len(fe.transcripts) == transcript_tags_number

    def test_removeTranscriptByEntry(self):
        fe = self.fe
        transcript_tags_number = len(fe.transcripts)

        t1 = Transcript(
            "https://examples.com/transcript_sample.txt",
            "text/html",
            language="es",
            is_caption=True,
        )
        fe.add_transcript(t1)
        assert len(fe.transcripts) == transcript_tags_number + 1

        fe.transcripts.remove(t1)
        assert len(fe.transcripts) == transcript_tags_number

    def test_settingLocation(self):
        self.fe.location = Location("My location")
        assert self.fe.location
        assert self.fe.location.text == "My location"
