# Modified work Copyright 2020, Thorben Dahl <thorben@sjostrom.no>
# Modified work Copyright 2021, Slim Beji <mslimbeji@gmail.com>
# See license.* for more details

lint:
	python venv/bin/black ./pod2gen
	python venv/bin/isort ./pod2gen

pip-upgrade:
	pip install --upgrade setuptools wheel twine

sdist:
	python setup.py sdist

wheel:
	python setup.py bdist_wheel --universal

clean: doc-clean
	@echo Removing binary files...
	@rm -f `find pod2gen -name '*.pyc'`
	@rm -f `find pod2gen -name '*.pyo'`
	@echo Removing source distribution files...
	@rm -rf dist/
	@rm -f MANIFEST

doc: doc-clean doc-html

doc-clean:
	@echo Removing docs...
	@make -C doc clean
	@rm -rf docs

doc-html:
	@echo 'Generating HTML'
	@make -C doc html
	@mkdir -p docs/html
	@echo 'Copying html to into docs dir'
	@cp -T -r doc/_build/html/ docs/html/

# Not supported
doc-man:
	@echo 'Generating manpage'
	@make -C doc man
	@mkdir -p docs/man
	@echo 'Copying manpage to into docs dir'
	@cp doc/_build/man/*.1 docs/man/

# Not supported
doc-latexpdf:
	@echo 'Generating pdf'
	@make -C doc latexpdf
	@mkdir -p docs/pdf
	@echo 'Copying pdf to into docs dir'
	@cp doc/_build/latex/*.pdf docs/pdf/

publish: pip-upgrade lint sdist wheel
	twine upload dist/*

test:
	@venv/bin/python -W ignore -m unittest pod2gen.tests.test_podcast pod2gen.tests.test_episode \
		pod2gen.tests.test_person pod2gen.tests.test_media pod2gen.tests.test_alternate_media \
		pod2gen.tests.test_util pod2gen.tests.test_category  pod2gen.tests.test_location \
		pod2gen.tests.test_funding pod2gen.tests.test_license pod2gen.tests.test_transcript \
		pod2gen.tests.test_trailer pod2gen.tests.test_soundbite pod2gen.tests.test_documentation
	@venv/bin/python -m pod2gen rss > /dev/null
